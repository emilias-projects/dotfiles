# Switch to a different kubernetes namespace
kn() {
    if [ "$1" != "" ]; then
            kubectl config set-context --current --namespace=$1
    else
            echo -e "\e[1;31m Error, please provide a valid Namespace\e[0m"
    fi
}

# Switch to the default kuberentes namespace
knd() {
    kubectl config set-context --current --namespace=default
}

# Unset current Kubernetes Context
ku() {
    kubectl config unset current-context
}

# Colormap
function colormap() {
  for i in {0..255}; do print -Pn "%K{$i}  %k%F{$i}${(l:3::0:)i}%f " ${${(M)$((i%6)):#3}:+$'\n'}; done
}

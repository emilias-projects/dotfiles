# Aliases
alias k="kubectl"
alias h="helm"
alias v="vagrant"
alias tf="terraform"
alias a="ansible"
alias ap="ansible-playbook"

# ALIAS COMMANDS
alias ls="exa --group-directories-first"
alias lsa="exa --group-directories-first -al"
alias ll="exa --group-directories-first -l"
#alias g="goto"
alias grep="grep --color"
alias code="code-insiders"
